# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

require Rails.root.join('common/common.rb')
Dir[Rails.root.join('common/app/**/*.rb')].each do |file|
  app_path = file.gsub('common/', '')
  require app_path if File.exists? app_path
end