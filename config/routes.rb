Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  match 'active'  => 'admin/sessions#active',  via: :get
  match 'timeout' => 'admin/sessions#timeout', via: :get

  namespace :admin do
    get '/' => 'users#dashboard'

    resources :users do
      collection do
        get :dashboard
      end
    end

    resources :sessions do
      collection do
        get :login
        post :login_attempt
        get :logout
      end
    end
    resources :orders, only: [:index, :show, :update, :create, :new, :edit] do
      collection do
        get :download_order
        put :tracking
        get :tracking_modal
        get :paid
        get :download_excel
        get :order_detail_excel
        put :add_delivery
      end
    end
    resources :roles
    resources :permissions
    resources :product_stock do
      collection do
        get :download_excel
      end
    end
    resources :customers do
      collection do
        put :verify
        get :download_excel
        post :import
      end
    end
    #resources :customers_imports, only: [:new, :create]
    resources :product_offers do
      collection do
        post :upload_offer_image
      end
    end

    resources :products, only: [:index, :show, :update, :create, :new, :edit] do
      collection do
        post :upload_product_image
        get :download_excel
        post :import
      end
    end

    resources :product_divisions, only: [:index, :show, :update, :create, :edit, :new] do
      collection do
        post :upload_division_image
      end
    end

    resources :product_sections, only: [:index, :show, :update, :create, :edit, :new]  do
      collection do
        post :upload_section_image
      end
    end

    resources :product_departments, only: [:index, :show, :update, :create, :edit, :new]

    resources :coupons do
      collection do
        put :redeem_coupon
        post :generate_coupons
      end
    end

    resources :debtors, only: [:index, :show] do
      collection do
        get :add_entry
        post :create_entry
        post :upload_entries
        get :refresh_debtors
        get :multiple_business_accounts
        put :reconcile_account
      end
    end

    resources :notification do
      collection do
        post :send_portal_notification
      end
    end

    resources :reports do
      collection do
        get :item_reports
        get :salesperson_reports
        get :customer_reports
        get :download_item_reports
      end
    end

    resources :schemes
    resources :salesman_tracking
  end

  namespace :api do
    resources :users do
      collection do
        get :existing_user
      end
    end

    resources :orders do
      collection do
        get :customer_orders
        get :user_orders
        get :paid
        get :view_coupon
      end
    end

    resources :products do
        collection do
          get :items
          get :search_items
          get :division_names
          get :get_items
          get :offer_items
        end
    end
    resources :customers do
      collection do
        get :customer_coupons
        get :ledger
      end
    end
    resources :linograph_offers do
      collection do
        get :b2b
        get :b2c
      end
    end

    resources :coupons,  only: [] do
      collection do
        get :view_coupon
        get :redeem_coupon
      end
    end

    resources :schemes
    resources :salesman_tracking do
      collection do
        get :salesman
        get :customer
      end
    end
  end
end
