require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'roo'
require 'csv'
require 'iconv'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SalesManagement
  class Application < Rails::Application

    config.exceptions_app = self.routes
    config.autoload_paths += Dir[Rails.root.join('lib'), Rails.root.join('common')]
    config.eager_load_paths += Dir[Rails.root.join('lib')]
    # Mailer queue
    require_relative '../lib/global_constant'
    # add yml path for genrated locale files
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.yml').to_s]

    config.action_dispatch.default_headers['X-Frame-Options'] = 'ALLOWALL'
    config.time_zone = 'Kolkata'
    config.active_record.default_timezone = :local
  end
end
