class NotificationService
  FIREBASE_API_KEY = "AAAAOXDUSgo:APA91bG8lXSFn7J5PZKBVltmK3cuFMtd7xf9zrFNHJljxXPgpS-WIzTUSXOLYGnBYpgpqvS2BXznSQt1aVNZRvQBXa-dsucyn2YTl5QgGdXCMbG90dHf6vsXVU9MYQ1dm5gscJSWmw2L"

  def initialize(data)
    @fcm = FCM.new(FIREBASE_API_KEY)
    @data = data
  end

  def send_notification
    user_token = "/topics/#{@data.try(:phone)}"
    data = {
        "data": {
            "title": "Order Updated",
            "body": "Your order has been #{@data.order_status.titleize}",
            "order_status": @data.order_status,
            "order_id": @data.id,
        }
    }
        response = @fcm.send_with_notification_key(user_token, data)
        Rails.logger.info "#{response.to_s}"
  end

  def portal_notification
    if @data['phone'].present?
      user_token = "/topics/#{@data['phone']}"
    elsif @data['section'].present?
      user_token = "/topics/#{@data['section']}"
    else
      user_token = "/topics/Linograph"
    end
    data = {
        "data": {
            "title": "#{@data['title']}",
            "body": "#{@data['body']}",
        }
    }
    if @data['image'].present?
      hash = {
          "image": "#{@data['image']}"
      }
      data[:data] = data[:data].merge(hash)
    end
    response = @fcm.send_with_notification_key(user_token, data)
    Rails.logger.info "#{response.to_s}"
  end
end