class CustomerCouponsService

  def initialize(customer_id)
    @customer_id = customer_id
  end

  def add_coupon(initials, amount, description, minimum_inovice_value, expired_in_days, coupon_type="fixed_value")
    customer_id = @customer_id
    loop do
      code =  initials.to_s.upcase + Array.new(6){[*"A".."Z", *"0".."9"].sample}.join
      coupon = Coupon.new({code: code, amount: amount, description: description, created_by: 20 , minimum_invoice_value: minimum_inovice_value, expired_at: Date.today + expired_in_days.days, coupon_type: coupon_type})
      if coupon.save
        CustomerCoupon.create({customer_id: @customer_id, coupon_id: coupon.id})
        break
      end
    end
    #Food Studio
    # add_deals("CAF", nil, "30% off on min order of Rs 1200", 1200, 30, 29, @customer_id, 2)
    # # Rudy Lounge
    # add_deals("CAF", nil, "40% off on min order of Rs 600", 600, 30, 30, @customer_id, 2)
    #Talab Cafe
   #  add_deals("CAF", nil, "100 Rs off on min order of Rs 350", 350, 30, 31, @customer_id, 2)
   #  add_deals("CAF", nil, "Order 2 snacks and get shakes/mocktail free", nil, 30, 31, @customer_id, 2)
   #  add_deals("CAF", nil, "50% off on min order of Rs 500", 500, 30, 31, @customer_id, 2)
   #  # #Cafe NH 72
   #  # add_deals("CAF", nil, "Buy any large pizza and get garlic bread and coke free", nil, 30, 32, @customer_id, 2)
   #  # add_deals("CAF", nil, "20% off on min order of Rs 300 (11am - 3pm)", 300, 30, 32, @customer_id, 2)
   #  # add_deals("CAF", nil, "Ice tea and burger @ Rs 139", nil, 30, 32, @customer_id, 2)
   #  # #	G7 pub and bistro
   #  add_deals("G7P", nil, "Buy 2 Get 1 Free Starter", nil, 30, 33, customer_id, 2, "<b>Terms And Conditions</b><ul><li>Offer cannot be clubbed with any other.</li><li>In case of any dispute, final dicission will be of Business owner. </li><li>This offer can be stopped anytime by the management without prior information.</li><li>This coupon is to be presented physically before billing. </li><li>Offer not valid on Fri, Sat, Sun </li></ul>")
   #  add_deals("G7P", nil, "2 Veg Starter + 4 Soft Drinks + 1 Sheesha For Rs 800 only", nil, 30, 33, customer_id, 2, "<b>Terms And Conditions</b><ul><li>Offer cannot be clubbed with any other.</li><li>In case of any dispute, final dicission will be of Business owner. </li><li>This offer can be stopped anytime by the management without prior information.</li><li>This coupon is to be presented physically before billing. </li></ul>")
   #  add_deals("G7P", nil, "Buy 1 Get 1 On Shakes and Smoothies", 600, 30, 33, customer_id, 2,"<b>Terms And Conditions</b><ul><li>Offer cannot be clubbed with any other.</li><li>In case of any dispute, final dicission will be of Business owner. </li><li>This offer can be stopped anytime by the management without prior information.</li><li>This coupon is to be presented physically before billing. </li><li>Offer not valid on Fri, Sat, Sun </li><li>Offer valid before 8:00 PM</li></ul>")
   #  # #Coding Blocks
   # #HUH
   #  add_deals("COD", nil, "1 Mini waffle free on order above 179", 179, 30, 36, customer_id, 3)
   #  add_deals("COD", nil, "1 exotic choclate shake and veg/chicken burger at Rs129", nil, 30, 36, customer_id, 3)
  end


  def add_deals(initials, amount, description, minimum_inovice_value, expired_in_days, created_by, customer_id, coupon_type,tnc="<b>Terms And Conditions</b><ul><li>Offer cannot be clubbed with any other.</li><li>In case of any dispute, final dicission will be of Business owner. </li><li>This offer can be stopped anytime by the management without prior information.</li><li>This coupon is to be presented physically before billing. </li></ul>")
    loop do
      code =  initials.to_s.upcase + Array.new(6){[*"A".."Z", *"0".."9"].sample}.join
      coupon = Coupon.new({code: code, amount: amount, description: description, created_by: created_by , minimum_invoice_value: minimum_inovice_value, expired_at: Date.today + expired_in_days.days, coupon_type: coupon_type, terms_and_conditions: tnc})
      if coupon.save
        CustomerCoupon.create({customer_id: customer_id, coupon_id: coupon.id})
        break
      end
    end
  end

end