module ConvertorsService
  class DebtorEntries

    def initialize(file)
      @error_records = []
      @file_path = file.path
    end

    def convert_and_upload
      file_object = Roo::Spreadsheet.open(@file_path)
      errors = {}
      file_object.drop(1).each do |row|
        customer = Customer.find_by(name: row[2])
        if customer.present?
          debtor = Debtor.find_by(customer_id: customer.id)
          debtor = Debtor.create({customer_id: customer.id, total_debit: 0, total_credit: 0, sales_return: 0, opening_balance: 0, outstanding_balance: 0}) unless debtor.present?
          row[1] = row[1].to_i unless row[1].to_i == 0
          entry = DebtorLedger.find_or_initialize_by({order_reference: row[1], payment_mode: 9})
          unless entry.id.present?
            entry.update({debtor_id: debtor.id, date: row[0], mode: 1, debit_amount: row[3].to_f, credit_amount: 0})
            debtor.update({total_debit: debtor.total_debit + row[3].to_f,outstanding_balance: debtor.outstanding_balance + row[3].to_f})
          else
            errors[row] = "An entry with given reference and payment mode already exist"
          end
        else
          errors[row] = "Customer with given name not present"
        end
      end

      #DebtorEntriesMailer.send_email(errors).deliver_now if errors.present?
    end
  end
end
