class OrderMailer < ApplicationMailer
  default from: "suvidhareports@gmail.com"

  def order_mail(order)
    @order = order
    mail(to: @order.customer.email, subject: 'Order Placed')
  end
end
