module CouponsHelper

  def admin_users_for_select
    User.all.pluck(:name, :id).to_a
  end

  def companies_for_select
    Customer.all.pluck(:company, :id).to_a
  end

  def customers_for_select
    Customer.all.pluck(:company, :phone_number).to_a
  end

  def salesperson_for_select
    User.all.pluck(:name, :id).to_a
  end

end