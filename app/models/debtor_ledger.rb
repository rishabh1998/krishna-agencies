class DebtorLedger < ApplicationRecord
  enum mode: {dr: 1, cr: 2}
  enum payment_mode: {cash: 1, cheque: 2, credit_card: 3, adjustment: 4, neft_rtgs: 5, paytm: 6, sales_return: 7, others: 8, sales_invoice: 9, opening_balance: 10, reverse_adjustment: 11}
  belongs_to :updated_by_user, class_name: :AdminUser, foreign_key: :updated_by
end
