class Offer < ApplicationRecord
  has_many :offer_products
  has_many :products, through: :offer_products
  accepts_nested_attributes_for :offer_products, allow_destroy: true

  enum status: { active: 1, inactive: 2 }
  enum offer_type: { b2b: 1, b2c: 2 }
end
