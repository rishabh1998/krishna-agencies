class Debtor < ApplicationRecord
  belongs_to :customer, foreign_key: :customer_id
  has_many :debtor_ledgers

  belongs_to :updated_by_user, class_name: :User, foreign_key: :updated_by
end
