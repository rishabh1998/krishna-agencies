class RedeemedCoupon < ApplicationRecord
  belongs_to :coupon
  belongs_to  :user, class_name: 'User', foreign_key: :redeemed_by

  ransacker :created_at do
    Arel.sql("date(redeemed_coupons.created_at)")
  end
end
