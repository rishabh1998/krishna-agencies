class Customer < ApplicationRecord

  enum status: { active: 1, inactive: 2 }
  enum customer_type: {B2B: 1, B2C: 2}

  validates :username, :uniqueness => true, on: :create
  validates :phone_number, :uniqueness => true, on: :create
  validates_length_of :phone_number, is: 10,  message: "must be 10 digit long"
  validates_length_of :gstin, is: 15,  message: "must be 15 digit long", if: Proc.new { |a| a.B2B? }
  #validates_length_of :password, :in => 6..20, :on => :create
  has_many :orders
  has_many :order_products
  has_many :customer_coupons
  has_many :coupons, through: :customer_coupons
  has_one :debtor
  before_create :create_username

  def create_username
    if self.B2B?
      self.username = "cus_" + self.phone_number
    else
      self.username = "b2c_" + self.phone_number
    end

  end

  def self.import(file)
    accessible_attributes = ["name", "phone_number", "email", "address",	"company",	"gstin",	"account_number",	"ifsc",	"customer_type",	"status"]
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      #row["gstin"] = row["gstin"].to_i unless row["gstin"].to_i == 0
      row["phone_number"] = row["phone_number"].to_i unless row["phone_number"].to_i == 0
      row["account_number"] = row["account_number"].to_i unless row["account_number"].to_i == 0
      #row["ifsc"] = row["ifsc"].to_i unless row["ifsc"].to_i == 0
      customer = find_by_phone_number(row["phone_number"]) || new
      customer.attributes = row.to_hash.slice(*accessible_attributes)
      customer.verification = true
      customer.save!
      unless customer.debtor.present?
        Debtor.create(customer_id: customer.id, updated_by: 1)
        CustomerCouponsService.new(customer.id).add_coupon('NEW', 101, "WELCOME EaseMYSales", 2199, 28)
      end
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

end
