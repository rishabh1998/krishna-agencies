class Order < ApplicationRecord
  belongs_to :customer
  belongs_to :user,optional: true
  has_many :order_products
  has_many :products, through: :order_products
  accepts_nested_attributes_for :order_products, allow_destroy: true
  has_one :order_tracking
  belongs_to :coupon

  enum order_status: { new_order: 1, accepted:2, dispatched: 3, delivered: 4, cancelled: 5, partially_delivered: 6}
  enum is_paid: {false: 0, paid_by_customer: 1, paid_by_admin: 2}

  after_create :redeem_coupon
  after_update :add_referral

  validates_presence_of :customer_id

  accepts_nested_attributes_for :order_products, reject_if: :all_blank

  def redeem_coupon
    if coupon_id.present?
      RedeemedCoupon.create({coupon_id: coupon_id, redeemed_by: 1, redeemed_for: "to be updated"})
    end
  end

  def add_referral
    if order_status == 'delivered'
      customer_account = customer
      if customer_account.referred_by.present? && customer_account.orders.where(orders: {order_status: 'delivered'}).count == 1
        referred_customer = Customer.find_by({id: customer_account.referred_by})
        CustomerCouponsService.new(referred_customer.id).add_coupon('REF', 75, "REFFERAL BONUS", 999, 60) if referred_customer.present?
      end
    end
  end

  ransacker :created_at do
    Arel.sql("date(orders.created_at)")
  end
end


