class Scheme < ApplicationRecord
  enum status: { active: 1, inactive: 2 }
end
