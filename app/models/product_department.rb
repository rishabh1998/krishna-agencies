class ProductDepartment < ApplicationRecord
  has_many :products
  belongs_to :created_by_user, class_name: :User, foreign_key: :created_by
  belongs_to :updated_by_user, class_name: :User, foreign_key: :updated_by
  enum status: {active: 1, inactive: 2}
end