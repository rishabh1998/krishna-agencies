class Coupon < ApplicationRecord
  belongs_to  :user, class_name: 'User', foreign_key: :created_by
  has_one :redeemed_coupon
  has_one :customer_coupon
  has_one :order, foreign_key: :coupon_id
  accepts_nested_attributes_for :redeemed_coupon
  validates_uniqueness_of :code

  enum coupon_type: {fixed_value: 1, percentage_value: 2}

  ransacker :created_at do
    Arel.sql("date(coupons.created_at)")
  end
end
