class Product < ApplicationRecord

  # Associations
  belongs_to :product_division, foreign_key: :product_division_id
  belongs_to :product_section, foreign_key: :product_section_id
  belongs_to :product_department
  belongs_to :created_by_user, class_name: :User, foreign_key: :created_by
  belongs_to :updated_by_user, class_name: :User, foreign_key: :updated_by
  enum status: { active: 1, inactive: 2 }

  # Validations
  validates :article_number, :uniqueness => true
  enum unit_type: {tin: 1, packet: 2, jar: 3}

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |product|
        csv << product.attributes
      end
    end
  end

  def self.import(file)
    accessible_attributes = ["name", "article_number", "product_division_id", "product_section_id", "product_department_id",	"mrp",	"min_rsp",	"max_rsp",	"discount", "special_discount", "stock_quantity", "status", "product_description"]
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      row["article_number"] = row["article_number"].to_i unless row["article_number"].to_i == 0
      product = find_by_article_number(row["article_number"]) || new
      row["product_division_id"] = ProductDivision.find_by(name: row["product_division"]).id
      row["product_section_id"] = ProductSection.find_by(name: row["product_section"]).id
      row["product_department_id"] = ProductDepartment.find_by(name: row["product_department"]).id
      product.attributes = row.to_hash.slice(*accessible_attributes)
      product.save!
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

end
