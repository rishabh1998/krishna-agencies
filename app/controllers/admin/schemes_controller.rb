class Admin::SchemesController < ApiController
  before_action :authenticate_user

  def index
    @q = Scheme.ransack(params[:q])
    @schemes = @q.result.paginate(page: params[:page], per_page: 10)
  end

  def new
    @scheme = Scheme.new
  end

  def create
    @scheme = Scheme.new(cat_params)
    if @scheme.save
      set_flash_notification :success, :create, entity: 'Scheme'
      redirect_to admin_schemes_path
    else
      set_instant_flash_notification :danger, :default, {:message => @scheme.errors.messages[:base][0]}
      render :new
    end
  end

  def edit
    @scheme = Scheme.find(params[:id])
  end

  def update
    @scheme = Scheme.find(params[:id])
    if @scheme.update(cat_params)
      set_flash_notification :success, :update, entity: 'Scheme'
      redirect_to admin_schemes_path
    else
      set_instant_flash_notification :danger, :default, {:message => @scheme.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :edit
    end
  end

  private

  def cat_params
    params.require(:scheme).permit(:title, :description, :status)
  end
end