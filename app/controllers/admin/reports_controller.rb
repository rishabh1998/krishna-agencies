class Admin::ReportsController < ApiController
  before_action :authenticate_user

  def index

  end

  def item_reports
    @q = Order.ransack(params[:q])
    @orders = @q.result
    @order_products = OrderProduct.where(order_id: @orders.ids)
    @products = Product.includes(:product_division).all
  end

  def salesperson_reports
    if params[:q].nil?
      params[:q] = {"created_at_lteq" => Time.now().strftime("%Y-%m-%d"), "created_at_gteq" => (Time.now() - 10.day).strftime("%Y-%m-%d")}
    end
    @q = Order.where.not(:user_id => nil).ransack(params[:q])
    @orders = @q.result
    @order_products = OrderProduct.where(order_id: @orders.ids)
    @invoice = {}
    @quantity = {}
    params[:q]["created_at_gteq"].upto(params[:q]["created_at_lteq"]) do |date|
      date = Date.parse(date) rescue nil
      if !date.nil?
        @invoice[date] = @order_products.where(:created_at => date.midnight..date.end_of_day).sum('rsp * quantity')
        @quantity[date] = @order_products.where(:created_at => date.midnight..date.end_of_day).sum('quantity')
      end
    end
  end

  def customer_reports
    if params[:q].nil?
      params[:q] = {"created_at_lteq" => Time.now().strftime("%Y-%m-%d"), "created_at_gteq" => (Time.now() - 10.day).strftime("%Y-%m-%d")}
    end
    @q = Order.ransack(params[:q])
    @orders = @q.result
    @order_products = OrderProduct.where(order_id: @orders.ids)
    @invoice = {}
    @quantity = {}
    params[:q]["created_at_gteq"].upto(params[:q]["created_at_lteq"]) do |date|
      date = Date.parse(date) rescue nil
      if !date.nil?
        @invoice[date] = @order_products.where(:created_at => date.midnight..date.end_of_day).sum('rsp * quantity')
        @quantity[date] = @order_products.where(:created_at => date.midnight..date.end_of_day).sum('quantity')
      end
    end
  end

  def download_item_reports
    @order_products = OrderProduct.where(order_id: params[:ids])
    @products = Product.includes(:product_division).all
    render xlsx: "item_reports", template: "/admin/reports/item_reports.xls.axlsx"
  end

end