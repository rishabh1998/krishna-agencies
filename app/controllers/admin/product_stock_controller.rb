class Admin::ProductStockController < ApiController
  before_action :authenticate_user

  def show
  end

  def index
    @q = Product.ransack(params[:q])
    @products = @q.result.page(params[:page])
  end

  def download_excel
    @q = Product.ransack(params[:q])
    @products = @q.result
    render xlsx: "stock", template: "/admin/product_stock/stock.xls.axlsx"
  end
  
  def edit
    @product =Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    stock = product_params["stock_quantity"].to_i
    @product.stock_quantity = @product.stock_quantity.to_i+stock
    if @product.save
      set_flash_notification :success, :update, entity: 'Stock'
      redirect_to action: "index"
    else
      set_instant_flash_notification :danger, :default, {:message => @product.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :edit
    end
  end

  private
  def product_params
    params.require(:product).permit(:stock_quantity)
  end
end