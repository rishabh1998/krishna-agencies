class Admin::ProductsController < ApiController
  before_action :authenticate_user
  require 'csv'
  def index
    @q = Product.includes(:product_division, :product_section, :product_department,:created_by_user, :updated_by_user).ransack(params[:q])
    @products = @q.result.where("name LIKE '%#{params[:name2_cont]}%'").order(updated_at: :desc).paginate(page: params[:page], per_page: 100)
  end

  def download_excel
    @q = Product.includes(:product_division, :product_section, :product_department,:created_by_user, :updated_by_user).ransack(params[:q])
    @products = @q.result.where("name LIKE '%#{params[:name2_cont]}%'").order(updated_at: :desc)
    render xlsx: "products", template: "/admin/products/products.xls.axlsx"
  end

  def import
    Product.import(params[:file])
    set_flash_notification :success, :default, {:message => "Products updated using Excel"}
    redirect_to admin_products_path
  end

  def show
  end

  def new
    @product = Product.new
  end

  def update
    product = Product.find(params[:id])
    if product.update(product_params.merge(updated_by: @current_user.id))
      set_flash_notification :success, :update, entity: 'Product'
      redirect_to admin_products_path
    else
      set_instant_flash_notification :danger, :default, {:message => @product.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      redirect_to admin_products_path
    end
  end

  def create
    @product = Product.new(product_params.merge({created_by: @current_user.id, updated_by: @current_user.id}))
    if @product.save
      set_flash_notification :success, :create, entity: 'Product'
      redirect_to admin_products_path
    else
      set_instant_flash_notification :danger, :default, {:message => @product.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def upload_product_image
    UploadToS3Service.new(params["document_#{params[:article_number]}"], params[:article_number], {extension: 'jpg', public_read: true, upload_folder: "products"}).update_file
    Product.find_by(article_number: params[:article_number]).try(:touch)
    set_flash_notification :success, :update, entity: 'Product Image'
    redirect_to admin_products_path
  end

  private

  def product_params
    params.require(:product).permit(:product_section_id, :product_department_id, :product_division_id, :name, :article_number, :mrp, :discount, :special_discount, :rsp, :min_rsp, :max_rsp, :stock_quantity, :product_description, :status)
  end
end
