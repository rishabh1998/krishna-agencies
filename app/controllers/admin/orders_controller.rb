class Admin::OrdersController < ApiController
  before_action :authenticate_user

  def index
    @q = Order.includes(:customer,:user,:order_products).ransack(params[:q])
    @orders = @q.result.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
  end

  def download_excel
    @q = Order.includes(:customer,:user,:order_products).ransack(params[:q])
    @orders = @q.result.order(created_at: :desc)
    render xlsx: "orders", template: "/admin/orders/orders.xls.axlsx"
  end

  def order_detail_excel
    @order = Order.find(params[:id])
    render xlsx: "order_details", template: "/admin/orders/order_detail.xls.axlsx"
  end

  def show
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    if order_params["order_products_attributes"].present?
      partial = false
      order_params["order_products_attributes"].each do |order_product|

        current = @order.order_products.find(order_product[1]["id"].to_i)
        current.dispatched_quantity+= order_product[1]["dispatched_quantity"].to_i
        if(current.quantity != current.dispatched_quantity)
          partial = true
        end
        stock = current.product.stock_quantity - order_product[1]["dispatched_quantity"].to_i
        current.product.update(:stock_quantity => stock)
        current.save
      end
      if partial
        @order.order_status = "partially_delivered"
      else
        @order.order_status = "dispatched"
      end
      entity = 'Order Delivery'
    else
      @order.attributes = order_params
      entity = 'Order Status'
    end
    if @order.order_status_changed?
      NotificationService.new(@order).send_notification
      #if @order.order_status == "dispatched"
      #  @order.order_products.each do |order_product|
      #  product = Product.find(order_product.product_id)
      #  product.stock_quantity-=order_product.quantity
      #  product.save
      #  end
      #end
    end
    if @order.save
      set_flash_notification :success, :update, entity: entity
      redirect_to admin_orders_path
    else
      set_instant_flash_notification :danger, :default, {:message => @order.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :edit
    end
  end

  def tracking_modal
    @tracking = OrderTracking.where(order_id: params[:id]).first_or_initialize
  end

  def tracking
    @tracking = OrderTracking.where(order_id: tracking_params["order_id"].to_i).first_or_initialize
    @tracking.attributes = tracking_params
    if @tracking.save
      set_flash_notification :success, :update, entity: 'Order Tracking'
      redirect_to admin_orders_path
    else
      set_instant_flash_notification :danger, :default, {:message => @order.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :edit
    end
  end

  def download_order
    @order = Order.find(params[:id])

    render   pdf: "order_"+@order.id.to_s,
             locals: { order: @order },
             template: 'admin/orders/order.pdf.erb',
             disposition: 'attachment',
             formats: :html, encoding: 'utf8'
    #send_file pdf, filename: 'whatever.pdf',type: "application/pdf"
  end

  def paid
    order = Order.find(params[:id])
    order.is_paid = "paid_by_admin"
    if order.save
      set_flash_notification :success, :default, {:message => "Order Marked as paid"}
      redirect_back(fallback_location: "admin/orders/#{order.id}")
    else
      set_flash_notification :danger, :default, {:message => "Error Marking as paid"}
      redirect_back(fallback_location: "admin/orders/#{order.id}")
    end
  end

  private
  def order_params
    params.require(:order).permit(:order_status, :order_track_id,  order_products_attributes: [:id, :dispatched_quantity])
  end

  def tracking_params
    params.require(:order_tracking).permit(:order_id, :tracking_id, :transport_name, :no_of_pkg, :weight, :freight, :tracking_url)
  end
end