class Admin::ProductOffersController < ApiController
  before_action :authenticate_user

  def index
    @q = Offer.ransack(params[:q])
    @offers = @q.result.paginate(page: params[:page], per_page: 10)
  end

  def new
    @offer = Offer.new
  end

  def create
    @offer = Offer.new(cat_params)
    if @offer.save
      set_flash_notification :success, :create, entity: ' Offer'
      redirect_to admin_product_offers_path
    else
      set_instant_flash_notification :danger, :default, {:message => @offer.errors.messages[:base][0]}
      render :new
    end
  end

  def edit
    @offer = Offer.find(params[:id])
  end

  def update
    @offer = Offer.find(params[:id])
    if @offer.update(cat_params)
      set_flash_notification :success, :update, entity: ' Offer'
      redirect_to admin_product_offers_path
    else
      set_instant_flash_notification :danger, :default, {:message => @offer.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :edit
    end
  end

  def upload_offer_image
    UploadToS3Service.new(params["document_#{params[:id]}"], params[:id], {extension: 'jpg', public_read: true, upload_folder: "offers"}).update_file
    Offer.find_by(id: params[:id]).try(:touch)
    set_flash_notification :success, :update, entity: 'Offer Image'
    redirect_to admin_product_offers_path
  end

  private

  def cat_params
    params.require(:offer).permit(:name, :description, :order, :offer_type, :status, product_ids: [])
  end
end