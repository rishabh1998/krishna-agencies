class Admin::CustomersController < ApiController
  before_action :authenticate_user

  def index
    @q = Customer.ransack(params[:q])
    @customers = @q.result.page(params[:page])
  end

  def download_excel
    @q = Customer.ransack(params[:q])
    @customers = @q.result
    render xlsx: "customer", template: "/admin/customers/customers.xls.axlsx"
  end

  def import
    Customer.import(params[:file])
    set_flash_notification :success, :default, {:message => "Customers updated using Excel"}
    redirect_to admin_customers_path
  end

  def new
    @customer = Customer.new
  end

  def create
    @customer = Customer.new(customer_params)
    @customer.verification = true
    if @customer.save
      Debtor.create(customer_id: @customer.id, updated_by: 1)
      CustomerCouponsService.new(@customer.id).add_coupon('NEW', 101, "WELCOME EaseMYSales", 2199, 28)
      set_flash_notification :success, :create, entity: 'Customer'
      redirect_to admin_customers_path
    else
      set_instant_flash_notification :danger, :default, {:message => @customer.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :new
    end
  end

  def edit
    @customer = Customer.find(params[:id])
  end

  def update
    @customer = Customer.find(params[:id])
    if @customer.update(customer_params)
      set_flash_notification :success, :update, entity: 'Customer'
      redirect_to admin_customers_path
    else
      set_instant_flash_notification :danger, :default, {:message => @customer.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :edit
    end
  end

  def verify
    @customer = Customer.find_by(id: params[:id])
    if @customer.update(verification: true)
      set_flash_notification :success, :update, entity: 'Customer'
      redirect_to admin_customers_path
    end
  end

  private
  def customer_params
    params.require(:customer).permit(:name, :address, :phone_number, :email, :company, :gstin, :account_number, :ifsc, :status, :verification, :customer_type)
  end

end

