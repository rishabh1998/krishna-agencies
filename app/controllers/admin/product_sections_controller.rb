class Admin::ProductSectionsController < ApiController
  before_action :authenticate_user
  def index
    @q = ProductSection.ransack(params[:q])
    @sections = @q.result.order(updated_at: :desc).paginate(page: params[:page], per_page: 30)
  end

  def show
  end

  def new
    @section = ProductSection.new
  end

  def update
    section = ProductSection.find(params[:id])
    if section.update(section_params.merge(updated_by: @current_user.id))
      set_flash_notification :success, :update, entity: 'Product Section'
      redirect_to admin_product_sections_path
    else
      set_flash_notification :error, :update, message: "#{section.errors}"
      redirect_to admin_product_sections_path
    end
  end

  def create
    @section = ProductSection.new(section_params.merge(:created_by=>@current_user.id,:updated_by=>@current_user.id))
    if @section.save
      set_flash_notification :success, :create, entity: 'Product Section'
      redirect_to admin_product_sections_path
    else
      set_instant_flash_notification :danger, :default, {:message => @section.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :new
    end
  end

  def edit
    @section = ProductSection.find(params[:id])
  end

  def upload_section_image
    UploadToS3Service.new(params["document_#{params[:id]}"], params[:id], {extension: 'jpg', public_read: true, upload_folder: "sections"}).update_file
    ProductSection.find_by(id: params[:id]).try(:touch)
    set_flash_notification :success, :update, entity: 'section Image'
    redirect_to admin_product_sections_path
  end

  private
  def section_params
    params.require(:product_section).permit(:name, :description, :status)
  end
end
