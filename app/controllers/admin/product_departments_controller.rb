class Admin::ProductDepartmentsController < ApiController
  before_action :authenticate_user
  def index
    @q = ProductDepartment.ransack(params[:q])
    @departments = @q.result.order(updated_at: :desc).paginate(page: params[:page], per_page: 30)
  end

  def show
  end

  def new
    @department = ProductDepartment.new
  end

  def update
    department = ProductDepartment.find(params[:id])
    if department.update(department_params.merge(updated_by: @current_user.id))
      set_flash_notification :success, :update, entity: 'Product Department'
      redirect_to admin_product_departments_path
    else
      set_flash_notification :error, :update, message: "#{department.errors}"
      redirect_to admin_product_departments_path
    end
  end

  def create
    @department = ProductDepartment.new(department_params.merge(:created_by=>@current_user.id,:updated_by=>@current_user.id))
    if @department.save
      set_flash_notification :success, :create, entity: 'Product Department'
      redirect_to admin_product_departments_path
    else
      set_instant_flash_notification :danger, :default, {:message => @department.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :new
    end
  end

  def edit
    @department = ProductDepartment.find(params[:id])
  end

  private
  def department_params
    params.require(:product_department).permit(:name, :description, :status)
  end
end