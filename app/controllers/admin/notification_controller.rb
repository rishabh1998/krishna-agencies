class Admin::NotificationController < ApiController
  before_action :authenticate_user

  def index
  end

  def show
  end

  def send_portal_notification
    @data = params.require(:data).permit(:title, :phone, :body, :image, :to, :section)
    image = ""
    if params[:data][:file].present?
      UploadToS3Service.new(params[:data][:file], "notification", {extension: 'jpg', public_read: true, upload_folder: "products"}).update_file
      image =  URI.encode("https://s3-ap-south-1.amazonaws.com/ems.images/products/notification.jpg?timestamp=#{Time.now().to_s}")
    end
    @data[:image] = image
    if NotificationService.new(@data).portal_notification
      set_flash_notification :success, :create, {:message => "Notification sent"}
    else
      set_flash_notification :danger, :create, {:message => "Unable to send Notification"}
    end
    redirect_to :action => "index"
  end
end