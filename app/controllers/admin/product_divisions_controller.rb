class Admin::ProductDivisionsController < ApiController
  before_action :authenticate_user
  def index
    @q = ProductDivision.ransack(params[:q])
    @divisions = @q.result.order(updated_at: :desc).paginate(page: params[:page], per_page: 30)
  end

  def show
  end

  def new
    @division = ProductDivision.new
  end

  def update
    division = ProductDivision.find(params[:id])
    if division.update(division_params.merge(updated_by: @current_user.id))
      set_flash_notification :success, :update, entity: 'Product Division'
      redirect_to admin_product_divisions_path
    else
      set_flash_notification :error, :update, message: "#{division.errors}"
      redirect_to admin_product_divisions_path
    end
  end

  def create
    @division = ProductDivision.new(division_params.merge(:created_by=>@current_user.id,:updated_by=>@current_user.id))
    if @division.save
      set_flash_notification :success, :create, entity: 'Product Division'
      redirect_to admin_product_divisions_path
    else
      set_instant_flash_notification :danger, :default, {:message => @division.errors.messages.map{|k,v| "#{k} #{v.first}"}.first.humanize}
      render :new
    end
  end

  def edit
    @division = ProductDivision.find(params[:id])
  end

  def upload_division_image
    UploadToS3Service.new(params["document_#{params[:id]}"], params[:id], {extension: 'jpg', public_read: true, upload_folder: "divisions"}).update_file
    ProductDivision.find_by(id: params[:id]).try(:touch)
    set_flash_notification :success, :update, entity: 'division Image'
    redirect_to admin_product_divisions_path
  end

  private
  def division_params
    params.require(:product_division).permit(:name, :description, :status)
  end
end
