class Admin::DebtorsController < ApiController
  before_action :authenticate_user
  def index
    if !params[:q].present?
      @q = Customer.ransack
      @debtors = Debtor.includes(:customer).order(outstanding_balance: :desc).paginate(page: params[:page], per_page: 50)
      @paginate = true
    else
      @debtors = []
      @q = Customer.ransack(params[:q])
      customer_ids = @q.result.pluck(:id)
      @debtors = Debtor.includes(:customer).where(customer_id: customer_ids).to_a
      debtor_customer_ids = Debtor.where(customer_id: customer_ids).pluck(:customer_id)
      (customer_ids - debtor_customer_ids).each_with_index do |i, c_id|
        if i > 50
          break
        else
          @debtors << Debtor.new({customer_id: c_id})
        end
      end
      @paginate = false
    end

  end

  def multiple_business_accounts
    customer_ids = Customer.where(business_name: params[:name]).pluck(:id)
    @debtor_ledgers = DebtorLedger.includes(debtor: :customer).where(debtor_id: Debtor.where(customer_id: customer_ids))
  end

  def show
    if params[:q].present?
      params[:q][:debtor_id_eq] = params[:id]
    else
      params[:q] = {debtor_id_eq: params[:id]}
    end
    @q = DebtorLedger.ransack(params[:q])
    @debtor = Debtor.find(params[:id])
    @debtor_ledgers = @q.result.order(date: :desc)#.paginate(page: params[:page], per_page: 50)
  end

  def add_entry
    @customer = Customer.find(params[:customer_id])
  end

  def create_entry
    customer = Customer.find(params[:customer_id])
    debtor = Debtor.find_or_initialize_by(customer_id: customer.id)
    debit_credit_amount = set_debit_credit_amount
    total_debit = debtor.total_debit.to_f + debit_credit_amount[:debit_amount]
    total_credit = debtor.total_credit.to_f + debit_credit_amount[:credit_amount]

    order_reference = params[:order_reference].present? ? params[:order_reference] : "100#{(DebtorLedger.where("order_reference like '100%'").order(order_reference: :desc).limit(1).pluck(:order_reference).first.to_i.to_s.sub('100', '').to_i + 1).to_s.rjust(3, '0')}"
    entry = DebtorLedger.find_or_initialize_by({order_reference: order_reference, payment_mode: params[:payment_mode].to_i})
    #unless entry.id.present?
      bank = params[:bank].to_i == 0 ? nil: params[:bank].to_i
      entry.update({debtor_id: debtor.id, date: params[:date], order_reference: params[:order_reference], bank: bank,
                    mode: debit_credit_amount[:mode], debit_amount: debit_credit_amount[:debit_amount],
                    credit_amount: debit_credit_amount[:credit_amount], payment_mode: params[:payment_mode].to_i, comment: params[:comment], updated_by: @current_user.id})
      debtor.update({
                        total_debit: total_debit,
                        total_credit: total_credit,
                        sales_return: debtor.sales_return.to_f + debit_credit_amount[:sales_return],
                        outstanding_balance: total_debit - total_credit,
                        updated_by: @current_user.id
                    })
      set_flash_notification :success, :create, entity: "Customer Entry for  #{customer.name} added successfully"
   # else
    #  set_flash_notification :danger, :create, entity: "Pos Reference for given payment mode already exist"
    #end

    redirect_to_back_or_default
  end

  def upload_entries
    Convertors::DebtorEntriesJob.perform_now(params[:document])
    set_flash_notification :success, :create, message: "File Uploaded Successfully"
    redirect_to_back_or_default
  end

  def refresh_debtors
    Debtor.all.each do |debtor|
      debtor.total_credit = debtor.debtor_ledgers.sum(:credit_amount)
      debtor.total_debit = debtor.debtor_ledgers.sum(:debit_amount)
      debtor.outstanding_balance = debtor.total_debit - debtor.total_credit
      debtor.save
    end
    redirect_to_back_or_default
  end

  def reconcile_account
    entries = DebtorLedger.where(id: params[:ids])
    entries.update({reconciled: true, reconcile_comment: params[:reconcile_comment]})
    set_flash_notification :success, :create, message: "Updated Successfully"
    redirect_to_back_or_default({name: name})
  end

  private
  def set_debit_credit_amount
    if params[:payment_mode] == '9' || params[:payment_mode] == '11'
      debit_amount = params[:amount].to_f
      credit_amount = 0
      sales_return = 0
      mode = 1
    else
      mode = 2
      debit_amount = 0
      credit_amount = params[:amount].to_f
      if params[:payment_mode] == '7'
        sales_return = params[:amount].to_f
      else
        sales_return = 0
      end
    end
    {debit_amount: debit_amount, credit_amount: credit_amount, sales_return: sales_return, mode: mode}
  end
end

