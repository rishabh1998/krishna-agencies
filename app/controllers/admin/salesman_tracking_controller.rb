class Admin::SalesmanTrackingController < ApiController
  before_action :authenticate_user

  def index
    @q = SalesmanTracking.ransack(params[:q])
    @records = @q.result.page(params[:page])
  end
end