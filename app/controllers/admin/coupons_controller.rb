class Admin::CouponsController < ApiController
  before_action :authenticate_user

  def index
    if @current_user.can_manage_coupons?
      if params[:status] == "1"
        @q = Coupon.includes(:user, redeemed_coupon: [:user]).where.not(redeemed_coupons: {id: nil}).ransack(params[:q])
      elsif  params[:status] == "0"
        @q = Coupon.includes(:user, redeemed_coupon: [:user]).where(redeemed_coupons: {id: nil}).ransack(params[:q])
      else
        @q = Coupon.includes(:user, redeemed_coupon: [:user]).ransack(params[:q])
      end
    else
      @q = Coupon.includes(:user, redeemed_coupon: [:user]).where.not(redeemed_coupons: {id: nil}).ransack(params[:q])
    end
    @coupons = @q.result.paginate(page: params[:page], per_page: 10)
  end

  def generate_coupons
    CustomerCouponsService.new(params[:customer_id]).add_coupon(params[:initials], params[:amount], params[:description], params[:minimum_invoice_value], params[:expiry].to_i, params[:coupon_type].to_i)
    set_flash_notification :success, :create, entity: 'Coupon'
    redirect_to admin_coupons_path
  end

  def redeem_coupon
    coupon = Coupon.find_by({code: params[:code]})
    unless coupon.present?
      set_flash_notification :danger, :create, {message: "Coupon not present."}
    else
      if coupon.redeemed_coupon.present?
        set_flash_notification :danger, :create, {message: "Coupon already redeemed."}
      else
        redeem_coupon = RedeemedCoupon.new({coupon_id: coupon.id, redeemed_by: @current_user.id, redeemed_for: params[:order_id]})
        redeem_coupon.save!
        set_flash_notification :success, :create, {message: "Coupon redeemed successfully. For rs. #{coupon.amount}"}
      end
    end
    redirect_to admin_coupons_path
  end

end

