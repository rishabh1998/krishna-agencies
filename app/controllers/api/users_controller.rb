class Api::UsersController < ApiController

  def existing_user
    username = params[:username]
    if (username[0..2]=='usr')
      existing_user = User.find_by(username: username)
    else
      existing_user = Customer.find_by(username: username)
    end

    if existing_user.present?
      data =  {id: existing_user.id, phone: existing_user.phone_number, name: existing_user.name, address: existing_user.address, email: existing_user.email, company: existing_user.try(:company), gstin: existing_user.try(:gstin), verification: existing_user.try(:verification), status: existing_user.status}
      render json: {status: "SUCCESS", message: "user exists", data: data}
    else
      render json: {status: "ERROR", message: "User doesn't exists", code: "421"}
    end
  end
end