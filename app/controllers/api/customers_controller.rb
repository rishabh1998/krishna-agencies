class Api::CustomersController < ApiController

  def index
    customers = Customer.where(status: "active")
    render_collection(customers, Api::CustomersSerializer)
  end

  def create
    customer = Customer.new(customer_params)
    customer.status = "active"
    if customer.save
      Debtor.create(customer_id: customer.id, updated_by: 1)
      CustomerCouponsService.new(customer.id).add_coupon('NEW', 101, "WELCOME Linograph", 2199, 28)
      render_success(customer, "Customer Account Created sucessfully")
    else
        render_error({}, "Error in creating customer account")
    end
  end

  def ledger
    ledgers = Debtor.where(:customer_id => params[:customer_id])
    if ledgers.present?
      render_collection(ledgers, Api::DebtorSerializer)
    else
      render_error({}, "Can't find Debtor")
    end
  end

  def customer_coupons
    customer = Customer.find(params[:id])
    customer_coupons = customer.coupons.includes(:redeemed_coupon)
    render_collection(customer_coupons, Api::CouponsSerializer)
  end

  private
  def customer_params
    params.require(:customer).permit(:name, :address, :email, :phone_number, :company, :gstin, :account_number, :ifsc, :customer_type)
  end
end
