class Api::ProductsController < ApiController
  def index
   # if params[:grocery_offer_id].present?
   #   items = []
   #   items = OfferProduct.includes(product: [:product_division,:product_section, :product_department]).where(offer_id: params[:grocery_offer_id]).collect{|oi| oi.product if oi.product.view_in_app == true}.compact.sort_by{|p| p.name} if params[:page].to_i == 1
  #  else
      q = Product.includes(:product_division,:product_section, :product_department).active.ransack(params[:q])
      if params[:page] == 'all'
        items = q.result
      else
        items = q.result.paginate(page: params[:page], per_page: 50)
      end
    render_collection(items, Api::ItemsSerializer)
  end

  def items
    divisions = build_items
    render_success({divisions: divisions}, "Items", {})
  end

  def offer_items
    product_ids = OfferProduct.where(offer_id: params[:id]).pluck(:product_id)
    products = Product.where(id: product_ids).paginate(page: params[:page], per_page: 30)
    render_collection(products, Api::ItemsSerializer)
  end

  def get_items
    ids = params[:product_ids].try(:split, ",")
    items = Product.where(id: ids).active
    render_collection(items, Api::ItemsSerializer)
  end

  def division_names
    divisions = ProductDivision.active
    render_collection(divisions, Api::DivisionsSerializer)
  end

  def search_items
    items = Product.all.pluck(:name)
    render_success({products: items}, " Items for search", {})
  end

    private
    def build_items
      divisions = {}
      items = Product.includes(:product_division,:product_section, :product_department).active
      items.each do |item|
        if divisions[item.product_division_id].present?
          if divisions[item.product_division_id][:sections][item.product_section.id].present?
            unless divisions[item.product_division_id][:sections][item.product_section.id][:departments][item.product_department.id].present?
              divisions[item.product_division_id][:sections][item.product_section.id][:departments][item.product_department.id] = {product_department_id: item.product_department.id, product_department_name: item.product_department.name}
            end
          else
            divisions[item.product_division_id][:sections][item.product_section.id] = {product_section_id: item.product_section.id, product_section_name: item.product_section.name, product_section_image: "https://s3-ap-south-1.amazonaws.com/ems.images/sections/#{item.product_section.id}.jpg?timestamp=#{item.product_section.updated_at.to_s}" ,departments:
                {item.product_department.id=>{product_department_id: item.product_department.id, product_department_name: item.product_department.name}}}
          end
        else
          divisions[item.product_division_id] = {product_division_id: item.product_division.id, product_division_name: item.product_division.name, product_division_image: "https://s3-ap-south-1.amazonaws.com/ems.images/sections/#{item.product_division.id}.jpg?timestamp=#{item.product_division.updated_at.to_s}", sections:
              {item.product_section.id => {product_section_id: item.product_section.id, product_section_name: item.product_section.name, product_section_image: "https://s3-ap-south-1.amazonaws.com/ems.images/sections/#{item.product_section.id}.jpg?timestamp=#{item.product_section.updated_at.to_s}" ,departments:
                  {item.product_department.id=>{product_department_id: item.product_department.id, product_department_name: item.product_department.name}}}}}
        end
      end
      divisions = divisions.values
      divisions.each do |division|
        division[:sections] = division[:sections].values
        division[:sections].each do|section|
          section[:departments] = section[:departments].values
        end
      end
      divisions
    end
end


