class Api::CouponsController < ApiController
  def view_coupon
    coupon = Coupon.find_by({code: params[:code]})
    unless coupon.present?
      render_success({key: "-1"}, "", {})
    else
      if coupon.redeemed_coupon.present?
        render_success({key: "-9"}, "", {})
      else
        render_success({key: "#{coupon.amount};#{coupon.minimum_invoice_value}"}, "", {})
      end
    end
  end

  def redeem_coupon
    coupon = Coupon.find_by({code: params[:code]})
    unless coupon.present?
     #ExceptionMailerJob.perform_later({request: request.url, response: "-1", error: "Invalid Coupon"})
      render_success({key: "-1"}, "", {})
    else
      if coupon.redeemed_coupon.present?
       #ExceptionMailerJob.perform_later({request: request.url, response: "-1", error: "Expired Coupon"})
        render_success({key: "-9"}, "", {})
      else
        redeem_coupon = RedeemedCoupon.new({coupon_id: coupon.id, redeemed_by: params[:user_id], redeemed_for: params[:customer_id]})
        redeem_coupon.save!
        render_success({key: "1"}, "", {})
      end
    end
  end

end

