class Api::LinographOffersController < ApiController

  def b2b
    offers = Offer.active.where(offer_type: "b2b")
    render_collection(offers, Api::OffersSerializer)
  end

  def b2c
    offers = Offer.active.where(offer_type: "b2c")
    render_collection(offers, Api::OffersSerializer)
  end
end