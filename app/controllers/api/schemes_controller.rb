class Api::SchemesController < ApiController

  def index
    schemes = Scheme.active
    render_collection(schemes, Api::SchemesSerializer)
  end
end