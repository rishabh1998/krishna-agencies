class Api::OrdersController < ApiController

  def index
      orders = Order.includes(:user,:customer, order_products: [:product])
    render_collection(orders, Api::OrdersListSerializer)
  end

  def create
    order = Order.new(order_params)
    order.order_status = "new_order"
    if order.save
      OrderMailer.order_mail(order).deliver
      render_success({}, "Order created sucessfully", { order_id: order.id})
    else
      render_error({}, "Error in creating order ", { errors: order.errors.full_messages})
    end
  end

  def show
    orders = Order.includes(order_products: [:product]).where(id: params[:id])
    render_collection(orders, Api::OrdersListSerializer)
  end

  def customer_orders
    orders = Order.includes(:customer, order_products: [:product]).where(customer_id: params[:customer_id]).order(created_at: :desc)
    render_collection(orders, Api::OrdersListSerializer)
  end

  def user_orders
    orders = Order.includes(:user, order_products: [:product]).where(user_id: params[:user_id]).order(created_at: :desc)
    render_collection(orders, Api::OrdersListSerializer)
  end

  def paid
    order = Order.find(params[:id])
    order.is_paid = "paid_by_customer"
    if order.save
      render json: {success: "Success", code: "200", message: "Paid By customer"}
    else
      render json: {success: "Failed", code: "201", message: order.errors}
    end
  end

  def get_coupon

  end

  def view_coupon
    coupon = Coupon.find_by({code: params[:code]})
    if !coupon.present?
      render_success({status: "-1", message: "Coupon Not Present"}, "", {})
    elsif coupon.customer_coupon.try(:customer_id) != params[:customer_id].to_i
      render_success({status: "-1", message: "Coupon Not Valid for this user"}, "", {})
    elsif coupon.redeemed_coupon.present?
      render_success({status: "-1", message: "Coupon Already Redeemed"}, "", {})
    elsif coupon.expired_at < Date.today
      render_success({status: "-1", message: "Coupon Expired"}, "", {})
    else
      render_success({status: "1", coupon_details: {data: coupon}}, "", {})
    end
  end

  private
  def order_params
    params.require(:order).permit(:id,:user_id,:customer_id, :phone, :customer_address, :order_amount, :coupon_id, :comment, order_products_attributes: [:id, :product_id, :mrp, :discount, :rsp, :quantity])
  end

end