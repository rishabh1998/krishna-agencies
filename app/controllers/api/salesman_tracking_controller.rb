class Api::SalesmanTrackingController < ApiController

  def salesman
    records = SalesmanTracking.where(user_id: params[:salesman_id])
    render_collection(records, Api::SalesmanTrackingSerializer)
  end

  def customer
    records = SalesmanTracking.where(customer_id: params[:customer_id])
    render_collection(records, Api::SalesmanTrackingSerializer)
  end

  def create
    record = SalesmanTracking.new(record_params)
    if record.save
      render json: {status: "SUCCESS", message: "Details saved", data: record}
    else
      render json: {status: "FAILED", message: "Details can't be saved", error: record.errors}
    end
  end

  private
  def record_params
    params.require(:salesman_tracking).permit(:user_id, :customer_id, :comment)
  end
end