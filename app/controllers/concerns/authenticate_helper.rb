module AuthenticateHelper
  extend ActiveSupport::Concern

  AUTH_CONFIG = YAML.load_file(open(Rails.root.to_s + '/config/secrets.yml'))[Rails.env]["api_auth"]
  AUTHENTICATE_ENV = %w(staging production testing)

  # Authenticate by checking if an encrypted timestamp is under api timeout limit
  #
  # Author By:: Raj
  # Date:: Thu Jul 14 18:48:30 2016
  # Reviewed By:: 
  #
  # Headers params Description
  # => hash_data: (request's body + timestamp in seconds) converted to digest using HMAC hashing algo in hex format.
  # => client_name: name of client of API(starfleet / NVM/ SF)

  def authenticate_api_token!
    if AUTHENTICATE_ENV.include?(Rails.env)
      current_time = (Time.current.to_f * 1000).to_i
      unless (digest_matches? && verify_by_timestamp(current_time))
        render json: { message: 'unauthorized' }, status: 401 and return false
      end
    end
  end

  private
    def verify_by_timestamp(current_time)
      (current_time - request.headers['Timestamp'].to_i) <= (AUTH_CONFIG["timeout"] * 1000)
    end

    def digest_matches?
      req_hash = request.headers['Hash-Data']
      client_name = request.headers['Client-Name']

      Rails.logger.info "Authenticating Request for client #{request.headers['Client-Name']}" \
        " with Request Hash: #{request.headers['Hash-Data']} and timestamp: #{request.headers['Timestamp']}"

      if req_hash.present? && client_name.present?
        req_hash == DigestService.new(client_name, request.headers['Timestamp'], request.raw_post).digest
      else
        false
      end
    end
end
