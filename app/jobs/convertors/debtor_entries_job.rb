class Convertors::DebtorEntriesJob < ActiveJob::Base
  def perform(document)
    ConvertorsService::DebtorEntries.new(document).convert_and_upload
  end
end