module Api
  class CustomersSerializer < ActiveModel::Serializer
    attributes :id, :name, :phone_number, :address, :gstin, :company, :account_number, :ifsc , :email, :verification
  end
end
