module Api
  class CouponsSerializer < ActiveModel::Serializer
    attributes :id, :code, :amount, :description, :minimum_invoice_value, :expired_at, :coupon_type, :name, :status, :phone, :address

    def minimum_invoice_value
	    object.minimum_invoice_value.to_i
    end

    def status
      val = 0
      val = 1 if object.redeemed_coupon.present?
      val = 2 if val != 1 && object.expired_at.present? && object.expired_at < Date.today
      val
    end

    def expired_at
      object.expired_at.try(:strftime, "%d-%b-%Y")
    end

    def name
      @user = User.find_by(id: object.created_by)
      @user.try(:name).to_s
    end

    def phone
      @user.try(:phone).to_s
    end

    def address
      @user.try(:address).to_s
    end

    def terms_and_conditions
      object.terms_and_conditions.to_s
    end
  end
end

