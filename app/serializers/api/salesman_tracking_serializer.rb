module Api
  class SalesmanTrackingSerializer < ActiveModel::Serializer
    attributes :id, :user_id, :customer_id, :comment
  end
end
