module Api
  class OffersSerializer < ActiveModel::Serializer
    attributes :id, :image, :offer_type

    def image
      return "https://s3-ap-south-1.amazonaws.com/ems.images/offers/#{object.id}.jpg?timestamp=#{object.updated_at.to_s}"
    end
  end
end
