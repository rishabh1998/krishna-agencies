module Api
  class DebtorSerializer < ActiveModel::Serializer
    attributes :id, :customer, :total_debit, :total_credit, :sales_return, :outstanding_balance, :ledgers

    def customer
      customer = object.customer
      {
          name: customer.name,
          company: customer.company,
          gstin: customer.gstin
      }
    end

    def ledgers
      ledgers = object.debtor_ledgers
    end
  end
end
