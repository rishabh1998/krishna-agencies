module Api
  class DivisionsSerializer < ActiveModel::Serializer
    attributes :id, :name, :image

    def image
      return "https://s3-ap-south-1.amazonaws.com/ems.images/divisions/#{object.id}.jpg"
    end
  end
end
