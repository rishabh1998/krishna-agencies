module Api
  class ItemsSerializer < ActiveModel::Serializer
    attributes :id, :division, :section, :department, :name, :mrp, :rsp, :discount, :special_discount, :image, :max_rsp, :min_rsp, :max_count, :stock_quantity, :product_description


    def name
      object.name.titleize
    end

    def division
      {
          id: object.product_division.id,
          name: object.product_division.name.titleize
      }
    end

    def section
      {
        id: object.product_section.id,
        name: object.product_section.name.titleize

      }
    end

    def rsp
      object.mrp * (100 - object.discount)/100
    end

    def department
      {
          id: object.product_department.id,
          name: object.product_department.name.titleize
      }
    end

    def image
      "https://s3-ap-south-1.amazonaws.com/ems.images/products/#{object.article_number}.jpg?timestamp=#{object.updated_at.to_s}"
    end

    def max_count
      10
    end

  end
end
