module Api
  class OrdersListSerializer < ActiveModel::Serializer

    attributes :id,:user_details, :customer_details, :order_details, :order_status, :order_tracking, :is_paid, :coupon_applied

    def user_details
      user = object.user
      {
          id: user.try(:id),
          name: user.try(:name),
          phone_number: object.phone
      }
    end

    def customer_details
      customer = object.customer
      {
          id: customer.try(:id),
          name: customer.try(:name),
          phone_number: object.phone,
          address: object.customer_address
      }
    end

    def order_details
      products = []
      discount = 0
      object.order_products.each do |order_product|
        discount = discount.to_i + order_product.discount.to_i
        products << {name: order_product.product.try(:name), mrp:  order_product.product.try(:mrp), discount: order_product.product.try(:discount), rsp: (order_product.product.mrp.to_f * (100 - order_product.product.try(:discount).to_f)/100).round(2), quantity:  order_product.quantity, delivered_quantity: order_product.dispatched_quantity, image_url:  "https://s3-ap-south-1.amazonaws.com/ems.images/products/#{order_product.id}.jpg?timestamp=#{order_product.updated_at.to_s}"}
      end
      {
          products: products,
          order_amount: object.order_amount,
          discount: discount,
          total_amount: object.order_amount.to_i + discount,
          date: object.created_at.strftime('%d-%m-%Y')

      }
    end

    def order_tracking
      tracking = object.order_tracking
      {
          tracking_id: tracking.try(:tracking_id),
          transport_name: tracking.try(:transport_name),
          no_of_pkg: tracking.try(:no_of_pkg),
          weight: tracking.try(:weight),
          freight: tracking.try(:freight),
          tracking_url: tracking.try(:tracking_url)
      }
    end

    def coupon_applied
      coupon = object.coupon
      {
          coupon_id: coupon.try(:id),
          code: coupon.try(:code),
          coupon_type: coupon.try(:coupon_type),
          amount: coupon.try(:amount),
          description: coupon.try(:description)
      }
    end
  end
end

