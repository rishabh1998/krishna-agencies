module Api
  class SchemesSerializer < ActiveModel::Serializer
    attributes :id, :title, :description, :status
  end
end
