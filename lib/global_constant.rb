# encoding: utf-8
module GlobalConstant

  # Load environment specific constants
  env_constants = YAML.load_file(Rails.root.to_s + '/config/constants.yml')['constants']
  backend_config = env_constants['backend']
  backend_api = env_constants['backend']['api']

  # ENV
  ENV_NAME = env_constants['caroobi']['env_name']
  ADMIN_MAIL_PREFIX = "STARFLEET [caroobi: #{GlobalConstant::ENV_NAME}]"

  # Redis configuration
  REDIS_URL = env_constants['redis']['url']

  # Phone Validator API configuration
  AUTHORIZED_TIME_RANGE = env_constants['caroobi']['authorized_time_frame']

  # Locales Configuration
  AVAILABLE_LOCALES = {
      en_DE: 'English - Germany',
      de_DE: 'German - Germany'
  }

  DEFAULT_BACKEND_LOCALE = 'en_DE'
  DEFAULT_APP_LOCALE = 'de_DE'

  # App configuration
  # ToDo: Sort these out
  APP_URL = env_constants['app']['url']

  APP_BE_API_URL = env_constants['app']['backend_api_url']
  BACKEND_API_URL = "#{backend_config['url']}#{backend_api['path']}/#{backend_api['version']}"
  BACKEND_CLIENT_ID = backend_api['client_id']
  BACKEND_CLIENT_SECRET = backend_api['client_secret']

  # Default locale for translations
  DEFAULT_LOCALE = env_constants['caroobi']['default_locale']

  # Translation Sheets Map
  # id: identifier for the sheet in translations.yml file Google Spreadsheet ID
  # key: Google Spreadsheet key
  TRANSLATION_SHEETS_MAP = [
      {
          key: '17Wumuo9SNwno8rlqfw0hLvHpriZ3tUGCfSv7cVGjB_A',
          id: 'app'
      },
      {
          key: '12Cw5BwUTWqip8EzZlY2YORlseygTxS2WpsNAVl7i9ec',
          id: 'iframes'
      }
  ]

  # Pricing Sheets Map
  # id: identifier for the sheet in pricing.yml file Google Spreadsheet ID
  # key: Google Spreadsheet key
  PRICING_DATA_SHEETS_MAP = [
      {
          key: '1QoM_WZis7rqh2QhUOlQJBrovmlchaxqwrfWdeg_2Ac4',
          id: 'area_code'
      },
      {
          key: '1dSgL0z3Y7m-EFTe6_q9VNqFcfTMykJNgBauLdhSLLz4',
          id: 'window_tinting'
      }
  ]

  # json request headers
  JSON_HEADERS = {
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
  }

  # Google App
  GOOGLE_CLIENT_ID = env_constants['google']['client_id']
  GOOGLE_CLIENT_SECRET = env_constants['google']['client_secret']

  # ToDo: Pull from env constants
  DEFAULT_PAGE_SIZE = 100

  # Resque queues
  RESQUE_QUEUES = {
      import_export_queue: :caroobi_import_export_queue,
      salesforce_requester_queue: :starfleet_salesforce_requester_queue
  }

  # Developer Emails
  CAROOBI_DEVS = env_constants['caroobi']['developer_emails']

  DEFAULT_CURRENCY = "EUR"
  CURRENCY = {
      "EUR" => '€'
  }

  SMTP = env_constants['smtp']

  S3 = env_constants['s3']

  S3_PUBLIC_PATH = "#{ S3['public_folder'] }/:class/:attachment/:id_partition/:style"
  S3_DEFAULT_PATH = "#{ S3['default_folder'] }/:class/:attachment/:id_partition/:style"


  # Lead Scoring
  TWILIO_CREDENTIAL = env_constants['twilio']
  PER_PAGE =  env_constants['lead_scoring']['per_page']
  FOC_LEAD_QUEUE_LOCK_TIME =  env_constants['lead_scoring']['foc_lead_queue_lock_time']

  REDIS_NAMESPACE = "caroobi_starfleet"
  CENTRAL_REDIS_NAMESPACE = "caroobi_shared"
  EMARSYS = env_constants['emarsys']

  ALLOW_EMARSYS_EMAIL = true

  EMAIL_REGEXP = /\A[a-z0-9._%+\-\/!#\$&'*=?^_`{|}~]+@[a-z0-9.-]+\.[a-z]+\z/i
  JS_EMAIL_REGEXP = "[A-Za-z0-9._%+\-\/!#\$&'*=?^_`{|}~]+@[A-Za-z0-9.-]+\.[a-zA-Z]+"

  TRANSLATION_KEYS_TABLES = ["TranslationDeDeKey", "TranslationEnDeKey"]
  TRANSLATION_KEYS_GROUPS = ["mistri"]

  # Lead Scoring
  FACTOR_OTHER_STR = 'Rest'
  FACTOR_OTHER_INT = -1

  # CMS
  CMS_PAGE_PREFIX = 'service'

  # map providers
  MAP_PROVIDER = env_constants['map_provider']
  BING_MAP_API = env_constants['bing']['api_key']

  BODY_TYPES_TO_ADD = ['Camper', 'wohnmobil']

  SALESFORCE_EMAIL = env_constants['caroobi_emails']['salesforce']

  MECHANIC_NUMBER_MECHANIC_ID = env_constants['mechanic_number']['mechanic_id']
  MECHANIC_NUMBER = env_constants['mechanic_number']['mechanic_number']
end
