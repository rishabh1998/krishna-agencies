class AddRemoveRenameColumns < ActiveRecord::Migration[5.2]
  def change
    rename_column :products, :stock_count, :stock_quantity
    add_column :customers, :verification, :integer, :default => 0
    remove_column :customers, :password
    remove_column :customers, :special_discount
  end
end
