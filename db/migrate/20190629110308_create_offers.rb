class CreateOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :offers do |t|
      t.string :name
      t.string :description
      t.integer :status
      t.integer :order

      t.timestamps
    end
  end
end
