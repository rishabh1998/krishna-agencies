class AddOrderTrackIdToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :order_track_id, :string
  end
end
