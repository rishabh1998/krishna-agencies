class AddColumnsToOrderProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :order_products, :dispatched_quantity, :integer, :default => 0
    add_column :order_products, :delivered_quantity, :integer, :default => 0
  end
end
