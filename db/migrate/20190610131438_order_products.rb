class OrderProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :order_products do |t|
      t.integer :order_id
      t.integer :product_id
      t.decimal :mrp, :precision => 15, :scale => 2
      t.decimal :discount, :precision => 15, :scale => 2
      t.decimal :rsp, :precision => 15, :scale => 2
      t.decimal :quantity, :precision => 15, :scale => 2

      t.timestamps
      t.index :order_id
    end
  end
end
