class CreateDebtors < ActiveRecord::Migration[5.2]
  def change
    create_table :debtors do |t|
      t.integer :customer_id
      t.float :total_debit
      t.float :total_credit
      t.float :sales_return
      t.float :opening_balance
      t.float :outstanding_balance
      t.integer :updated_by
      t.integer :last_payment_days_ago

      t.timestamps
      t.index :customer_id
    end
  end
end
