class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :username
      t.string :password
      t.string :phone_number
      t.string :company
      t.string :gstin
      t.string :account_number
      t.string :ifsc
      t.integer :status

      t.timestamps
    end
  end
end
