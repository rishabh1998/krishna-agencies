class AddSaltToCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :salt, :string
  end
end
