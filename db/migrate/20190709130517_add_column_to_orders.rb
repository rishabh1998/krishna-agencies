class AddColumnToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :is_paid, :integer, :default => 0
  end
end
