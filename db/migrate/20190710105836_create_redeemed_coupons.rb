class CreateRedeemedCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :redeemed_coupons do |t|
      t.integer :coupon_id
      t.integer :redeemed_by
      t.string :redeemed_for

      t.timestamps
      t.index :coupon_id
    end
  end
end
