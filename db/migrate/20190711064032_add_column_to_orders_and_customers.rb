class AddColumnToOrdersAndCustomers < ActiveRecord::Migration[5.2]
  def change
    add_reference :orders, :coupon, foreign_key: true
    add_column :customers, :referred_by, :integer
  end
end
