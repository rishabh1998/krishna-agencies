class CreateDebtorLedgers < ActiveRecord::Migration[5.2]
  def change
    create_table :debtor_ledgers do |t|
      t.integer :debtor_id
      t.datetime :date
      t.string :order_reference
      t.integer :mode
      t.float :debit_amount
      t.float :credit_amount
      t.integer :payment_mode
      t.float :net_balance
      t.text :comment
      t.integer :updated_by
      t.integer :bank
      t.boolean :reconciled, :default => false
      t.text :reconcile_comment

      t.timestamps
      t.index :debtor_id
    end
  end
end
