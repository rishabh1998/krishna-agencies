class Orders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string  :order_by
      t.integer :user_id
      t.integer :customer_id
      t.string :customer_address
      t.string :phone
      t.float :order_amount
      t.integer :order_status
      t.string  :order_reference_id
      t.timestamps

      t.index :customer_id
    end
  end
end
