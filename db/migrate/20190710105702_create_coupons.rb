class CreateCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :coupons do |t|
      t.string :code
      t.integer :amount
      t.string :description
      t.integer :created_by
      t.integer :minimum_invoice_value
      t.datetime :expired_at

      t.timestamps
      t.index :code
    end
  end
end
