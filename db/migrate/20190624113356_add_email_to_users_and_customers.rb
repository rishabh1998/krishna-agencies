class AddEmailToUsersAndCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :email, :string
    add_column :customers, :email, :string
  end
end
