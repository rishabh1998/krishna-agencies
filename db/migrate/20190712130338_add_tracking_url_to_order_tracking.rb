class AddTrackingUrlToOrderTracking < ActiveRecord::Migration[5.2]
  def change
    add_column :order_trackings, :tracking_url, :string
  end
end
