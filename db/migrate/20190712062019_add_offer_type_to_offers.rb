class AddOfferTypeToOffers < ActiveRecord::Migration[5.2]
  def change
    add_column :offers, :offer_type, :integer
  end
end
