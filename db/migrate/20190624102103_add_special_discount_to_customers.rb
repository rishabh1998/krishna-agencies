class AddSpecialDiscountToCustomers < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :special_discount, :integer
  end
end
