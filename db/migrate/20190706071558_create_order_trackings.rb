class CreateOrderTrackings < ActiveRecord::Migration[5.2]
  def change
    create_table :order_trackings do |t|
      t.references :order, foreign_key: true
      t.string :transport_name
      t.string :tracking_id
      t.integer :no_of_pkg
      t.decimal :weight, :precision => 15, :scale => 2
      t.decimal :freight, :precision => 15, :scale => 2

      t.timestamps
    end
  end
end
