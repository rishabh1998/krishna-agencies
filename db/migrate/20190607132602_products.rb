class Products < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.integer :product_section_id
      t.integer :product_division_id
      t.integer :product_department_id
      t.string :name
      t.string :barcode
      t.decimal :mrp, :precision => 15
      t.decimal :rsp, :precision => 15
      t.decimal :min_rsp, :precision => 15
      t.decimal :max_rsp, :precision => 15
      t.integer :discount, :precision => 15, :scale => 2
      t.integer :stock_count
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
      t.index [:id, :name, :barcode], name: "index_on_products", using: :btree
    end
  end
end
