class ProductDivisions < ActiveRecord::Migration[5.2]
  def change
    create_table :product_divisions do |t|
      t.string :name
      t.text :description
      t.integer :status
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
      t.index [:id, :name]
    end
  end
end
