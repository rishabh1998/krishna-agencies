class AddCouponTypeToCoupons < ActiveRecord::Migration[5.2]
  def change
    add_column :coupons, :coupon_type, :integer
  end
end
