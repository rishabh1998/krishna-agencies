class AddArticleNumberToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :article_number, :string
  end
end
