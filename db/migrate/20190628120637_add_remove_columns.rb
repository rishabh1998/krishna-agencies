class AddRemoveColumns < ActiveRecord::Migration[5.2]
  def change
    remove_column :customers, :verification
    add_column :customers, :verification, :boolean, :default => false
    add_column :customers, :customer_type, :integer
  end
end
