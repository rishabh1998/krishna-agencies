class ChangeQuantityToBeIntegerInOrderProducts < ActiveRecord::Migration[5.2]
  def change
    change_column :order_products, :quantity, :integer
  end
end
